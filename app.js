var createError = require('http-errors');
const express = require("./config/express");
require("dotenv").config();
process.env.NODE_ENV = process.env.NODE_ENV || "development";

var app = express();
const port = process.env.SERVER_PORT || 4000;
app.listen(port);
console.log("Express server listening on port " + port);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

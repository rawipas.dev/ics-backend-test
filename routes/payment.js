var express = require('express')
var router = express.Router()
const controller = require('../app/controllers/payment.controller')

// Routes
/**
 * @swagger
 * /payment/verify/:
 *  post:
 *    description: \[POST\] Customer verify payment order.
 *    tags:
 *      - Payment
 *    consumes:
 *      - multipart/form-data
 *    produces:
 *      - "application/json"
 *    parameters:
 *      - in: formData
 *        name: order_id
 *        type: integer
 *        example: 16
 *      - in: formData
 *        name: users_id
 *        type: integer
 *        example: 2
 *      - in: formData
 *        name: payment_date
 *        type: string
 *        example: "2021-12-26 12:21"
 *      - in: formData
 *        name: payment_price
 *        type: decimal
 *        example: 999.99
 *      - in: formData
 *        name: payment_bank
 *        type: string
 *        example: "creditcard"
 *      - in: formData
 *        name: files
 *        type: file
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Bad Required
 *      '400[1]':
 *        description: Missing file upload
 *      '400[2]':
 *        description: Not Found This Orders
 *      '401':
 *        description: Unauthorized
 *      '404':
 *        description: Not found route
 *      '500':
 *        description: Something Wrong
 */
router.post('/verify/', controller.verifyPayment)

module.exports = router

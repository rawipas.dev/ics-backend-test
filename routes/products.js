var express = require('express')
var router = express.Router()
const controller = require('../app/controllers/products.controller')

// Routes
/**
 * @swagger
 * /products/list/:
 *  post:
 *    description: \[POST\] Get Products List.
 *    tags:
 *      - Products
 *    consumes:
 *      - "application/json"
 *    produces:
 *      - "application/json"
 *    parameters:
 *      - in: body
 *        name: data
 *        schema:
 *          type: object
 *          properties:
 *            page:
 *              type: integer
 *              example: 1
 *            perPage:
 *              type: integer
 *              example: 10
 *            gender:
 *              type: string
 *              example: male
 *            category:
 *              type: integer
 *              example: 1
 *            size:
 *              type: string
 *              example: "XL"
 *    responses:
 *      '200':
 *        description: A successful response
 *        schema:
 *          type: object
 *          properties:
 *            success:
 *              type: boolean
 *              example: true
 *            response:
 *              type: object
 *              properties:
 *                  data:
 *                      type: object
 *                      properties:
 *                          product:
 *                              type: array
 *                              items:
 *                                  type: object
 *                                  properties:
 *                                      id:
 *                                          type: integer
 *                                          example: 999
 *                                      name:
 *                                          type: string
 *                                          example: "T-Shirt Dota2"
 *                                      gender:
 *                                          type: string
 *                                          example: "male"
 *                                      price:
 *                                          type: decimal
 *                                          example: 999.99
 *                                      category:
 *                                          type: string
 *                                          example: "Gamer"
 *                          page:
 *                              type: integer
 *                              example: 1
 *                          totalPage:
 *                              type: integer
 *                              example: 10
 *                          amountProduct:
 *                              type: integer
 *                              example: 1
 *                  code:
 *                     type: string
 *                     example: "200"
 *                  messages:
 *                     type: string
 *                     example: ""
 *      '400':
 *        description: Bad Required
 *      '401':
 *        description: Unauthorized
 *      '404':
 *        description: Not found route
 *      '500':
 *        description: Something Wrong
 */
router.post('/list/', controller.getProducts)

module.exports = router

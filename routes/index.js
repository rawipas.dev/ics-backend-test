var express = require('express');
var router = express.Router();
const controller = require('../app/controllers/test.controller')

/* GET home page. */
router.get('/', controller.test_controller);

module.exports = router;

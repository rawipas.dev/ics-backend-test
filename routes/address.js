var express = require('express')
var router = express.Router()
const controller = require('../app/controllers/address.controller')

// Routes
/**
 * @swagger
 * /address/create/:
 *  post:
 *    description: \[POST\] Customer Create Address.
 *    tags:
 *      - Address
 *    consumes:
 *      - "application/json"
 *    produces:
 *      - "application/json"
 *    parameters:
 *      - in: body
 *        name: data
 *        schema:
 *          type: object
 *          properties:
 *            users_id:
 *              type: integer
 *              example: 2
 *            name_address:
 *              type: string
 *              example: "ไร่มีชัย"
 *            recipient_fname:
 *              type: string
 *              example: "โชคดี"
 *            recipient_lname:
 *              type: string
 *              example: "มีชัย"
 *            recipient_tel:
 *              type: string
 *              example: "+66931456675"
 *            address:
 *              type: string
 *              example: "88 หมู่8 "
 *            address_subdistrict:
 *              type: integer
 *              example: 104301
 *            address_district:
 *              type: integer
 *              example: 43
 *            address_province:
 *              type: integer
 *              example: 1
 *            address_postcode:
 *              type: integer
 *              example: 10230
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Bad Required
 *      '401':
 *        description: Unauthorized
 *      '404':
 *        description: Not found route
 *      '500':
 *        description: Something Wrong
 */
router.post('/create/', controller.createAddress)

module.exports = router

var express = require('express')
var router = express.Router()
const controller = require('../app/controllers/orders.controller')

// Routes
/**
 * @swagger
 * /orders/create/:
 *  post:
 *    description: \[POST\] Create Orders.
 *    tags:
 *      - Orders
 *    consumes:
 *      - "application/json"
 *    produces:
 *      - "application/json"
 *    parameters:
 *      - in: body
 *        name: data
 *        schema:
 *          type: object
 *          properties:
 *            user_id:
 *              type: integer
 *              example: 2
 *            address_id:
 *              type: integer
 *              example: 1
 *            products:
 *              type: array
 *              items:
 *                  type: object
 *                  properties:
 *                      products_variants_id:
 *                        type: integer
 *                      quantity:
 *                        type: integer
 *              example:
 *                 - products_variants_id: 1
 *                   quantity: 5
 *                 - products_variants_id: 2
 *                   quantity: 3
 *    responses:
 *      '200':
 *        description: A successful response
 *      '400':
 *        description: Bad Required
 *      '400[1]':
 *        description: Missing Users
 *      '400[2]':
 *        description: Missing Address
 *      '400[3]':
 *        description: Missing Products Variants
 *      '401':
 *        description: Unauthorized
 *      '404':
 *        description: Not found route
 *      '500':
 *        description: Something Wrong
 */
router.post('/create/', controller.createOrders)

// Routes
/**
 * @swagger
 * /orders/officer-get/:
 *  post:
 *    description: \[POST\] Officer Get Orders.
 *    tags:
 *      - Orders
 *    consumes:
 *      - "application/json"
 *    produces:
 *      - "application/json"
 *    parameters:
 *      - in: body
 *        name: data
 *        schema:
 *          type: object
 *          properties:
 *            page:
 *              type: integer
 *              example: 1
 *            perPage:
 *              type: integer
 *              example: 10
 *            start:
 *              type: string
 *              example: "2021-12-01"
 *            end:
 *              type: string
 *              example: "2021-12-31"
 *            status:
 *              type: integer
 *              example: 1
 *    responses:
 *      '200':
 *        description: A successful response
 *        schema:
 *          type: object
 *          properties:
 *            success:
 *              type: boolean
 *              example: true
 *            response:
 *              type: object
 *              properties:
 *                  data:
 *                      type: object
 *                      properties:
 *                          orders:
 *                              type: array
 *                              items:
 *                                  type: object
 *                                  properties:
 *                                      id:
 *                                          type: integer
 *                                          example: 999
 *                                      order_code:
 *                                          type: string
 *                                          example: "RSX208801310000099"
 *                                      order_price:
 *                                          type: decimal
 *                                          example: 100
 *                                      order_discount:
 *                                          type: decimal
 *                                          example: 0
 *                                      order_vat:
 *                                          type: decimal
 *                                          example: 7
 *                                      order_status:
 *                                          type: string
 *                                          example: "complete"
 *                                      status:
 *                                          type: integer
 *                                          example: 5
 *                                      users_id:
 *                                          type: integer
 *                                          example: 2
 *                                      customer_id:
 *                                          type: integer
 *                                          example: 1
 *                                      customer_name:
 *                                          type: string
 *                                          example: "นายมงคล ยอดภูผา"
 *                                      email:
 *                                          type: string
 *                                          example: "hello@mail.com"
 *                                      telephone:
 *                                          type: string
 *                                          example: "+66958473624"
 *                                      created_at:
 *                                          type: string
 *                                          example: "2088-01-31T00:00:00.990Z"
 *                                      modified_at:
 *                                          type: string
 *                                          example: "2088-01-31T00:00:00.990Z"
 *                          page:
 *                              type: integer
 *                              example: 1
 *                          totalPage:
 *                              type: integer
 *                              example: 10
 *                          amountOrders:
 *                              type: integer
 *                              example: 1
 *                  code:
 *                     type: string
 *                     example: "200"  
 *                  messages:
 *                     type: string
 *                     example: ""    
 *      '400':
 *        description: Bad Required
 *      '401':
 *        description: Unauthorized
 *      '404':
 *        description: Not found route
 *      '500':
 *        description: Something Wrong
 */
router.post('/officer-get/', controller.getOrdersByOfficer)

module.exports = router

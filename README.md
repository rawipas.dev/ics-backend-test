# ICS Test Backend

## Start Project 

```
# Run for Install Dependencies
npm install

# Run for Development (from branch develop)
npm run dev

# Run for Production
npm run start
```
## Connection Database
- host: 104.248.168.212
- database: test_db
- user: admin
- password: P@ssw0rd
- port: 5432

## API Document
if run dev will see api document at >>> [http://localhost:4000/api-docs/](http://localhost:4000/api-docs/)


***

## Description
Backend Developer Test Assignment (iCreativeSystems Co.,Ltd.)

## Installation
- [ ] [NodeJS/NPM v14.16.1+](https://nodejs.org/en/download/)


## Authors
***Rawipas Pacharoen*** :sunny:
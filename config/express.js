const express = require('express')
const morgan = require('morgan')
const dayjs = require('dayjs')
const utc = require('dayjs/plugin/utc')
const timezone = require('dayjs/plugin/timezone') // dependent on utc plugin
dayjs.extend(utc)
dayjs.extend(timezone)
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const flash = require('connect-flash')
const fileUpload = require('express-fileupload')
const path = require('path')
const SWAGGER_CONFIG = require("./swagger");

function handle_response(res, success, options = {}) {
  if (success) {
    let { code, messages, data } = options
    let response = {}
    if (messages) {
      response.messages = messages
    }

    if (data) {
      response.data = data
    }

    return res.json({
      success: true,
      response: {
        data: data,
        code: code,
        messages: messages,
      },
    })
  } else {
    let { code, messages, data } = options
    return res.json({
      success: false,
      response: {
        data,
        code: code,
        messages: messages,
      },
    })
  }
}

module.exports = function () {
  var app = express()
  app.use(fileUpload())
  morgan.token('date', (req, res, tz) => {
    return dayjs.tz(new Date(), 'Asia/Bangkok').format('DD-MM-YYYY HH:mm:ss')
  })
  morgan.format('dev', '[:date] ":method :url" :status :res[content-length] - :response-time ms')
  app.use(morgan('dev'))
  app.use(bodyParser.urlencoded({ extended: true }))
  app.use(express.json())
  app.use(cookieParser())
  app.use(express.static(path.join(__dirname, 'public')))
  app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*')
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE')
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization, language')
    res.setHeader('Access-Control-Allow-Credentials', true)

    res.handle_response = handle_response.bind(null, res)

    next()
  })

  app.set('view engine', 'html')

  app.use(flash())
  app.use(bodyParser.json())

  // require('../routes/index')(app)

  SWAGGER_CONFIG(app)
  app.use('/', require('../routes/index'))
  app.use('/products', require('../routes/products'))
  app.use('/orders', require('../routes/orders'))
  app.use('/address', require('../routes/address'))
  app.use('/payment', require('../routes/payment'))

  app.use(function (req, res, next) {
    if (req.method != 'OPTIONS') {
      res.status(404).send({ error: 'Not found' })
    } else {
      next()
    }
  })

  return app
}

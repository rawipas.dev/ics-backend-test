module.exports = {
  env: 'development',
  postgres: {
    user: process.env.POSTGRES_USER || 'admin',
    host: process.env.POSTGRES_HOST || '104.248.168.212',
    database: process.env.POSTGRES_DATABASE || 'test_db',
    password: process.env.POSTGRES_PASSWORD || 'P@ssw0rd',
    port: process.env.POSTGRES_PORT || '5432'
  },
  debug: process.env.DEBUG || 'true',
};
const swaggerJsDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')
const sub_domain = process.env.SUBDOMAIN || ''

module.exports = function (app) {
  const swaggerOptions = {
    swaggerDefinition: {
      info: {
        title: 'Test Backend API',
        description: 'Test Assignment Backend (by iCreativeSystems)',
        contact: {
          name: 'Developer',
        },
        servers: ['http://localhost:8080'],
      },
    },
    apis: ['routes/*.js'],
  }

  const option = {
    swaggerOptions: {
      defaultModelsExpandDepth: -1,
      defaultModelExpandDepth: -1,
      docExpansion: 'none',
    },
  }

  const swaggerDocs = swaggerJsDoc(swaggerOptions)
  app.use(`${sub_domain}/api-docs`, swaggerUi.serve, swaggerUi.setup(swaggerDocs, option))
}

const appRoot = require("app-root-path");
require("winston-daily-rotate-file");
const { createLogger, format, transports } = require("winston");
const { combine, timestamp, printf } = format;

const formatMsg = printf(({ data, level, message, timestamp }) => {
  let obj = ''
  if (typeof data === "object" || typeof data === "array") {
    obj = `echo. ${message}${JSON.stringify(data, null, 4)}`;
  }
  return `${timestamp} [${level}]: ${message} ${obj ? `\r\n________________\r\n ${obj}` : ''}`;
});
const formatTime = timestamp({
  format: "DD-MM-YYYY HH:mm:ss",
});

const dailyRotateError = new transports.DailyRotateFile({
  level: "error",
  name: "error-log",
  dirname: `${appRoot}/logs/error/`,
  filename: "error-log.%DATE%.log",
  datePattern: "DD-MM-YYYY",
  maxFiles: "15d",
});
const dailyRotateInfo = new transports.DailyRotateFile({
  level: "debug",
  name: "info-log",
  dirname: `${appRoot}/logs/info/`,
  filename: "info-log.%DATE%.log",
  datePattern: "DD-MM-YYYY",
  maxFiles: "15d",
});

const transConsole = new transports.Console({
  handleExceptions: true,
  json: false,
  colorize: true,
  timestamp: true,
  format: combine(
    format.prettyPrint(),
    format.colorize(),
    format.splat(),
    format.simple(),
    format.prettyPrint(),
    formatTime,
    formatMsg
  ),
});

dailyRotateError.on("rotate", function (oldFilename, newFilename) {
  console.log("----------------- ERRR ", oldFilename, newFilename);
});
dailyRotateInfo.on("rotate", function (oldFilename, newFilename) {
  console.log("----------------- INFO ", oldFilename, newFilename);
});

const logger = createLogger({
  format: combine(
    format.prettyPrint(),
    format.colorize(),
    format.splat(),
    format.simple(),
    format.prettyPrint(),
    formatTime,
    formatMsg
  ),
  transports: [dailyRotateError, dailyRotateInfo, transConsole],
  exitOnError: false,
});

if (process.env.NODE_ENV !== "production") {
  logger.add(new transports.Console({ level: "debug" }));
} else {
  logger.add(new transports.Console({ level: "info" }));
}

module.exports = { logger };

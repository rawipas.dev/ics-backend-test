const { logger } = require("./winston");
const get = require("lodash/get");

class Logger {
  constructor(path) {
    this.path = path;
  }

  debug(message, data) {
    let is_stack = check_stack(data);
    let is_obj = check_object(data);
    let log_message = {
      message:
        is_stack === null ? message : `${message} >>>>> ${is_stack} <<<<<`,
      class: this.path,
      data: is_obj ? JSON.stringify(data) : data,
      date: new Date(),
    };
    logger.debug(log_message);
  }
  info(message, data) {
    let is_stack = check_stack(data);
    let is_obj = check_object(data);
    let log_message = {
      message:
        is_stack === null ? message : `${message} >>>>> ${is_stack} <<<<<`,
      class: this.path,
      data: is_obj ? JSON.stringify(data) : data,
      date: new Date(),
    };
    logger.info(log_message);
  }
  verbose(message, data) {
    let is_stack = check_stack(data);
    let is_obj = check_object(data);
    let log_message = {
      message:
        is_stack === null ? message : `${message} >>>>> ${is_stack} <<<<<`,
      class: this.path,
      data: is_obj ? JSON.stringify(data) : data,
      date: new Date(),
    };
    logger.verbose(log_message);
  }
  error(message, data) {
    let is_stack = check_stack(data);
    let is_obj = check_object(data);
    let log_message = {
      message:
        is_stack === null ? message : `${message} >>>>> ${is_stack} <<<<<`,
      class: this.path,
      data: is_obj ? JSON.stringify(data) : data,
      date: new Date(),
    };
    logger.error(log_message);
  }
}

function check_stack(data) {
  if (get(data, "stack")) {
    return JSON.stringify(data.stack);
  } else if (get(data, "message")) {
    return data.message;
  } else {
    return null;
  }
}

function check_object(data) {
  if (typeof data === "object" || typeof data === "array") {
    return true;
  } else {
    return false;
  }
}

module.exports = Logger;

const approot = require("app-root-path");
const apppath = __filename.replace(approot, "");
const Logger = require(`${approot}/config/logger`);
const logger = new Logger(apppath);

module.exports = logger;

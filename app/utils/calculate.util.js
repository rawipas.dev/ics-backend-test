const Big = require('big.js');

const mul = (a, b) => {
  a = Number(a);
  b = Number(b);

  let x = new Big(a);
  let result = x.mul(b).toFixed(2);
  return Number(result);
};

const div = (a, b) => {
  a = Number(a);
  b = Number(b);

  let x = new Big(a);
  let result = x.div(b).toFixed(2);
  return Number(result);
};

const plus = (a, b) => {
  a = Number(a);
  b = Number(b);

  let x = new Big(a);
  let result = x.plus(b).toFixed(2);
  return Number(result);
};

const minus = (a, b) => {
  a = Number(a);
  b = Number(b);

  let x = new Big(a);
  let result = x.minus(b).toFixed(2);
  return Number(result);
};

module.exports = {
  mul,
  div,
  plus,
  minus
}

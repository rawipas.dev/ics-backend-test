const appRoot = require("app-root-path");
const logger = require(`${appRoot}/app/utils/logger.util`);

exports.test_controller = async function (req, res) {
  try {
    console.log('=== OK ===')
    logger.debug("##########[DEBUG] : AAAA");
    logger.info("##########[INFO] : SSSS");
    logger.verbose("##########[VERBOSE] : OOOOO");
    logger.error("##########[ERROR] : BBBB");

    return res.status(200).handle_response(true, {
      messages: "ไม่มีสิทธิเข้าใช้งานกรุณาเข้าสู่ระบบ",
      code: '200',
    })
  } catch (e) {
    console.error(e)
    logger.error('##########[ERROR] : ', e)
  }

  return res.status(500).send(false)
}

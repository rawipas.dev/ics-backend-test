const appRoot = require('app-root-path')
const logger = require(`${appRoot}/app/utils/logger.util`)
const dayjs = require('dayjs')
const sharp = require('sharp')
const fs = require('fs')
const db = require('../models')
const { Sequelize, Orders, Payment } = db
const Op = Sequelize.Op

exports.verifyPayment = async function (req, res) {
  try {
    const { order_id, users_id, payment_date, payment_price, payment_bank } = req.body

    if ((!order_id, !users_id, !payment_date, !payment_price, !payment_bank)) {
      return res.status(400).handle_response(false, {
        messages: 'missing information',
        code: '400',
      })
    }
    if (!req.files) {
      return res.status(400).handle_response(false, {
        messages: 'missing files upload',
        code: '400[1]',
      })
    }

    // * Check ID Order
    let checkOrders = await Orders.findOne({
      where: {
        id: order_id,
        users_id: users_id,
      },
    })
    if (!checkOrders) {
      return res.status(400).handle_response(false, {
        messages: 'Not Found This Order',
        code: '400[2]',
      })
    }

    let lastNight = new Date().setHours(0, 0, 0, 0)
    let NextNight = new Date().setHours(23, 59, 59, 9)
    let minDay = dayjs(lastNight).format()
    let maxDay = dayjs(NextNight).format()

    let file_name = 'IMG'

    // * Check last image from payment
    let checkPayment = await Payment.findAll({
      attributes: ['file_name', 'file_path'],
      order: [['created_at', 'DESC']],
      where: {
        created_at: {
          [Op.between]: [minDay, maxDay],
        },
      },
    })

    // * Generate file_name
    let toDay = dayjs(lastNight).format('YYYYMMDD')
    if (!checkPayment || checkPayment.length === 0) {
      file_name = `${file_name}${toDay}0000001`
    } else {
      let textCode = checkPayment[0].file_name
      let lastNumber = textCode.slice(11)
      let nextNumber = Number(lastNumber) + 1

      let textZero = ''
      for (let i = nextNumber.toString().length; i < 7; i++) {
        textZero += '0'
      }
      file_name = `${file_name}${toDay}${textZero}${nextNumber}`
    }

    let file_path = `upload/`
    if (!fs.existsSync(file_path)) {
      fs.mkdirSync(file_path)
    }

    // * Save File Upload 
    const { files } = req.files
    const imageSave = sharp(files.data)
    await imageSave.metadata().then(function (metadata) {
      return imageSave
        .resize(Math.round(metadata.width / 2))
        .toFormat('png')
        .png({ quality: 80 })
        .toFile(`${file_path}${file_name}.png`)
    })


    // * Create Payment Verify 
    let payDay = dayjs(payment_date).format()
    Payment.create(
      {
        order_id,
        payment_date: payDay,
        payment_price,
        payment_bank,
        file_name: file_name,
        file_path: `${file_path}${file_name}.png`,
        created_by: users_id,
        modified_by: users_id,
      },
      {
        fields: ['order_id', 'payment_date', 'payment_price', 'payment_bank', 'file_name', 'file_path', 'created_by', 'modified_by'],
      }
    )

    // * Update Status Orders
    await Orders.update(
      { status: 2 },
      {
        fields: ['status'],
        where: {
          id: order_id,
        },
      }
    )

    return res.status(200).handle_response(true, {
      messages: 'Upload Payment Success',
      code: '200',
    })
  } catch (e) {
    console.error(e)
    logger.error('##########[ERROR] : ', e)
    return res.status(500).handle_response(false)
  }
}

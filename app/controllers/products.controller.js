const appRoot = require('app-root-path')
const logger = require(`${appRoot}/app/utils/logger.util`)
const db = require('../models')
const { sequelize } = db

exports.getProducts = async function (req, res) {
  try {
    const { page = 1, perPage = 10, gender, category, size } = req.body

    const offset = !page ? 0 : perPage * (page - 1)
    const limit = !perPage ? 10 : perPage

    let condition = ''
    if (gender) {
      condition = `${condition} and p.gender = '${gender.toLowerCase()}'`
    }
    if (category) {
      condition = `${condition} and pc.id = ${category}`
    }
    if (size) {
      condition = `${condition} and pv."name" = '${size.toUpperCase()}'`
    }

    let sqlMain = `select p.id, p."name", p.gender, MIN(pv.price) as price,
    (select pc."name" from products_category pc where pc.id = p.category_id) as category
    from products p 
    left join products_category pc on pc.id = p.category_id 
    left join products_variants pv on pv.products_id = p.id 
    where p.visible = true and p.deleted = false and
    pv.visible = true and pv.deleted = false 
    ${condition} 
    GROUP by p.id 
    ORDER BY price`

    let sqlGetLimit = `${sqlMain} LIMIT ${limit} OFFSET ${offset}`

    let dataProducts = await sequelize.query(sqlGetLimit)
    let dataProductsAll = await sequelize.query(sqlMain)

    let total_page = Math.ceil(dataProductsAll[0].length / limit);
    let amount_product = dataProductsAll[0].length;
    
    const result = {
      product: dataProducts[0],
      page: page,
      totalPage: total_page,
      amountProduct: amount_product
    }

    return res.status(200).handle_response(true, {
      messages: '',
      code: '200',
      data: result,
    })
  } catch (e) {
    console.error(e)
    logger.error('##########[ERROR] : ', e)
  }

  return res.status(500).handle_response(false)
}

const appRoot = require('app-root-path')
const dayjs = require('dayjs')
const logger = require(`${appRoot}/app/utils/logger.util`)
const { plus, mul, minus } = require(`${appRoot}/app/utils/calculate.util`)
const db = require('../models')
const { sequelize, Sequelize, Users, Roles, Customers, Address, ProductsVariants, Orders, OrderProducts } = db
const Op = Sequelize.Op

exports.createOrders = async function (req, res) {
  try {
    const { user_id, address_id, products } = req.body

    // * Check request body
    if (!user_id || !address_id || products.length === 0) {
      return res.status(400).handle_response(false, {
        messages: 'missing information',
        code: '400',
      })
    }

    // * Check ID Users
    let checkUser = await Users.findOne({
      where: {
        enabled: true,
        deleted: false,
        id: user_id,
        roles: {
          [Op.not]: 2,
        },
      },
      include: [
        {
          model: Roles,
          as: 'role',
        },
        {
          model: Customers,
          as: 'cus',
        },
      ],
    })
    if (!checkUser) {
      return res.status(400).handle_response(false, {
        messages: 'missing users',
        code: '400[1]',
      })
    }

    // * Check ID Address
    let checkAddress = await Address.findOne({
      where: {
        deleted: false,
        id: address_id,
      },
    })
    if (!checkAddress) {
      return res.status(400).handle_response(false, {
        messages: 'missing address',
        code: '400[2]',
      })
    }

    // * Prepare Product for order_products
    let ordersProducts = await preProducts(products, user_id)
    if (!ordersProducts) {
      return res.status(400).handle_response(false, {
        messages: 'missing products variants',
        code: '400[3]',
      })
    }

    let dataOrders = await preOrders(ordersProducts, user_id, address_id)

    console.log('================= [dataOrders] =================', dataOrders)

    // * Save Orders to orders
    Orders.create(
      { ...dataOrders },
      { fields: ['order_code', 'users_id', 'address_id', 'order_price', 'order_discount', 'order_vat', 'status', 'created_by', 'modified_by'] }
    )
      .then(function (result) {
        // logger.debug('__________[DEBUG] : save order id[' + result.id + ']')
        let newOrderProd = ordersProducts.map((item) => ({ ...item, order_id: result.id }))

        // * Save Products to order_products
        OrderProducts.bulkCreate([...newOrderProd], {
          fields: [
            'order_id',
            'products_id',
            'products_variants_id',
            'products_price',
            'products_discount',
            'products_vat',
            'quantity',
            'created_by',
            'modified_by',
          ],
        })
          .then(async function (resOrderProd) {
            for (const data of resOrderProd) {
              let oldProdVar = await ProductsVariants.findOne({
                attributes: ['id', 'amount', 'amount_used'],
                where: {
                  id: data.products_variants_id,
                },
              })

              let newAmount = minus(oldProdVar.dataValues.amount, data.quantity)
              let newAmountUsed = plus(oldProdVar.dataValues.amount_used, data.quantity)

              await ProductsVariants.update(
                { amount: newAmount, amount_used: newAmountUsed },
                {
                  fields: ['amount', 'amount_used'],
                  where: {
                    id: data.products_variants_id,
                  },
                }
              )
            }
            // logger.debug('__________[DEBUG] : save Order Products')
          })
          .catch(function (error) {
            // error
            logger.error('##########[ERROR] : ', error)
          })
      })
      .catch(function (error) {
        // error
        logger.error('##########[ERROR] : ', error)
      })

    return res.status(200).handle_response(true, {
      messages: '',
      code: '200',
      data: {},
    })
  } catch (e) {
    console.error(e)
    logger.error('##########[ERROR] : ', e)
  }

  return res.status(500).handle_response(false)
}

async function preProducts(products, user_id) {
  let resultProduct = []
  for (const item of products) {
    // * Check ID Products Variants
    let checkProdVar = await ProductsVariants.findOne({
      where: {
        visible: true,
        deleted: false,
        id: item.products_variants_id,
      },
    })
    if (!checkProdVar) {
      return false
    }

    resultProduct.push({
      products_id: checkProdVar.products_id,
      products_variants_id: checkProdVar.id,
      products_price: checkProdVar.price,
      products_discount: checkProdVar.discount,
      products_vat: checkProdVar.vat,
      quantity: item.quantity,
      total_price: mul(item.quantity, checkProdVar.price), //price * quantity
      total_discount: mul(item.quantity, checkProdVar.discount), //discount * quantity
      total_vat: mul(item.quantity, checkProdVar.vat), //vat * quantity
      created_by: user_id,
      modified_by: user_id,
    })
  }

  return resultProduct
}

async function preOrders(products, user_id, address_id) {
  let lastNight = new Date().setHours(0, 0, 0, 0)
  let NextNight = new Date().setHours(23, 59, 59, 9)
  let minDay = dayjs(lastNight).format()
  let maxDay = dayjs(NextNight).format()

  let order_code = 'RSX'

  // * Check last order_code in today from Orders
  let checkOrders = await Orders.findAll({
    attributes: ['order_code', 'created_at'],
    order: [['created_at', 'DESC']],
    where: {
      created_at: {
        [Op.between]: [minDay, maxDay],
      },
    },
  })

  // * Generate order_code
  let toDay = dayjs(lastNight).format('YYYYMMDD')
  if (!checkOrders || checkOrders.length === 0) {
    order_code = `${order_code}${toDay}0000001`
  } else {
    let textCode = checkOrders[0].order_code
    let lastNumber = textCode.slice(11)
    let nextNumber = Number(lastNumber) + 1

    let textZero = ''
    for (let i = nextNumber.toString().length; i < 7; i++) {
      textZero += '0'
    }
    order_code = `${order_code}${toDay}${textZero}${nextNumber}`
  }

  let sumPrice = products.reduce((a, b) => ({
    total_price: plus(a.total_price, b.total_price),
    total_discount: plus(a.total_discount, b.total_discount),
    total_vat: plus(a.total_vat, b.total_vat),
  }))

  let orders = {
    order_code: order_code,
    users_id: user_id,
    address_id: address_id,
    order_price: sumPrice.total_price,
    order_discount: sumPrice.total_discount,
    order_vat: sumPrice.total_vat,
    order_total_price: minus(sumPrice.total_price, sumPrice.total_discount),
    status: 1,
    created_by: user_id,
    modified_by: user_id,
  }

  return orders
}

exports.getOrdersByOfficer = async function (req, res) {
  try {
    const { page = 1, perPage = 10, start, end, status } = req.body

    const offset = !page ? 0 : perPage * (page - 1)
    const limit = !perPage ? 10 : perPage

    let condition = ''
    if (status && !start && !end) {
      condition = `${condition} o.status = ${status}`
    } else if (start || end) {
      // ! สามารถ Filter ได้ด้วย ช่วงวันที่ของคำสั่งซื้อที่ชำระเงินแล้ว (Paid Date : Start - End)
      condition = ` o.status > 1 AND o.status <> 6 AND `
    }

    if (start && !end) {
      let startNight = new Date(start).setHours(0, 0, 0, 0)
      let minDay = dayjs(startNight).format()
      condition = `${condition} o.created_at > '${minDay}'`
    } else if (!start && end) {
      let endNight = new Date(end).setHours(23, 59, 59, 9)
      let maxDay = dayjs(endNight).format()
      condition = `${condition} o.created_at < '${maxDay}'`
    } else if (start && end) {
      let startNight = new Date(start).setHours(0, 0, 0, 0)
      let endNight = new Date(end).setHours(23, 59, 59, 9)
      let minDay = dayjs(startNight).format()
      let maxDay = dayjs(endNight).format()
      condition = `${condition} o.created_at BETWEEN '${minDay}' AND '${maxDay}'`
    }

    let sqlMain = `select 
    o.id, o.order_code, o.order_price, o.order_discount, o.order_vat, so."name" as order_status, o.status, 
    o.users_id, c.id as customer_id, CONCAT(c.fname,' ', c.lname) as customer_name,  u.email, c.telephone, 
    o.created_at, o.modified_at
    from orders o
    left join users u on u.id = o.users_id 
    left join customers c on c.users_id = u.id 
    left join status_orders so on so.id = o.status 
    where ${condition} 
    ORDER BY o.created_at DESC`

    let sqlGetLimit = `${sqlMain} LIMIT ${limit} OFFSET ${offset}`

    let dataOrders = await sequelize.query(sqlGetLimit)
    let dataOrdersAll = await sequelize.query(sqlMain)

    let total_page = Math.ceil(dataOrdersAll[0].length / limit)
    let amountOrders = dataOrdersAll[0].length

    const result = {
      orders: dataOrders[0],
      page: page,
      totalPage: total_page,
      amountOrders,
    }

    return res.status(200).handle_response(true, {
      messages: '',
      code: '200',
      data: result,
      page: page,
    })
  } catch (e) {
    console.error(e)
    logger.error('##########[ERROR] : ', e)
  }

  return res.status(500).handle_response(false)
}

const appRoot = require('app-root-path')
const logger = require(`${appRoot}/app/utils/logger.util`)
const db = require('../models')
const { sequelize, Address } = db

exports.createAddress = async function (req, res) {
  try {
    const {
      name_address,
      users_id,
      recipient_fname,
      recipient_lname,
      recipient_tel,
      address,
      address_subdistrict,
      address_district,
      address_province,
      address_postcode,
    } = req.body

    if (
      !name_address ||
      !users_id ||
      !recipient_fname ||
      !recipient_lname ||
      !recipient_tel ||
      !address ||
      !address_subdistrict ||
      !address_district ||
      !address_province ||
      !address_postcode
    ) {
      return res.status(400).handle_response(false, {
        messages: 'missing information',
        code: '400',
      })
    }

    Address.create(
      { ...req.body, created_by: users_id, modified_by: users_id },
      {
        fields: [
          'name_address',
          'users_id',
          'recipient_fname',
          'recipient_lname',
          'recipient_tel',
          'address',
          'address_subdistrict',
          'address_district',
          'address_province',
          'address_postcode',
          'created_by',
          'modified_by',
        ],
      }
    )

    return res.status(200).handle_response(true, {
      messages: 'Create Address Success',
      code: '200',
    })
  } catch (e) {
    console.error(e)
    logger.error('##########[ERROR] : ', e)

  }

  return res.status(500).handle_response(false)
}

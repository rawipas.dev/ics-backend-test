module.exports = (sequelize, Sequelize, options = {}) => {
  const Model = sequelize.define(
    'payment',
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
      },
      order_id: {
        type: Sequelize.INTEGER,
      },
      payment_date: {
        type: Sequelize.STRING,
      },
      payment_price: {
        type: Sequelize.DECIMAL,
      },
      payment_bank: {
        type: Sequelize.STRING,
      },
      file_name: {
        type: Sequelize.STRING,
      },
      file_path: {
        type: Sequelize.STRING,
      },
      deleted: {
        type: Sequelize.BOOLEAN,
      },
      created_at: {
        type: Sequelize.DATE,
      },
      created_by: {
        type: Sequelize.INTEGER,
      },
      modified_at: {
        type: Sequelize.DATE,
      },
      modified_by: {
        type: Sequelize.INTEGER,
      },
    },
    {
      freezeTableName: true,
      timestamps: false,
    }
  )

  return Model
}

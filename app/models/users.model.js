module.exports = (sequelize, Sequelize, options = {}) => {
  const { Roles, Customers } = options
  const Model = sequelize.define(
    'users',
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
      },
      email: {
        type: Sequelize.STRING,
      },
      roles: {
        type: Sequelize.INTEGER,
      },
      enabled: {
        type: Sequelize.BOOLEAN,
      },
      deleted: {
        type: Sequelize.BOOLEAN,
      },
      created_at: {
        type: Sequelize.DATE,
      },
      created_by: {
        type: Sequelize.INTEGER,
      },
      modified_at: {
        type: Sequelize.DATE,
      },
      modified_by: {
        type: Sequelize.INTEGER,
      },
    },
    {
      freezeTableName: true,
      timestamps: false,
    }
  )
  Model.hasMany(Roles, { as: 'role', foreignKey: 'id', sourceKey: 'roles' })
  Model.hasMany(Customers, { as: 'cus', foreignKey: 'users_id', sourceKey: 'id' })

  return Model
}

module.exports = (sequelize, Sequelize, options = {}) => {
  const Model = sequelize.define(
    'address',
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
      },
      name_address: {
        type: Sequelize.STRING,
      },
      users_id: {
        type: Sequelize.INTEGER,
      },
      recipient_fname: {
        type: Sequelize.STRING,
      },
      recipient_lname: {
        type: Sequelize.STRING,
      },
      recipient_tel: {
        type: Sequelize.STRING,
      },
      address: {
        type: Sequelize.STRING,
      },
      address_subdistrict: {
        type: Sequelize.INTEGER,
      },
      address_district: {
        type: Sequelize.INTEGER,
      },
      address_province: {
        type: Sequelize.INTEGER,
      },
      address_postcode: {
        type: Sequelize.INTEGER,
      },
      deleted: {
        type: Sequelize.BOOLEAN,
      },
      created_at: {
        type: Sequelize.DATE,
      },
      created_by: {
        type: Sequelize.INTEGER,
      },
      modified_at: {
        type: Sequelize.DATE,
      },
      modified_by: {
        type: Sequelize.INTEGER,
      },
    },
    {
      freezeTableName: true,
      timestamps: false,
    }
  )

  return Model
}

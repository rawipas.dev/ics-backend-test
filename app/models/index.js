const config = require('../../config/config')
const Sequelize = require('sequelize')
const sequelizeConfig = new Sequelize(config.postgres.database, config.postgres.user, config.postgres.password, {
  host: config.postgres.host,
  port: config.postgres.port,
  dialect: 'postgres',
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
  logging: true,
  // logging: false
})

Sequelize.postgres.DECIMAL.parse = function (value) {
  return Number(value)
}

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelizeConfig

let ProductsCategory = require('./products_category.model')(sequelizeConfig, Sequelize)
let Products = require('./products.model')(sequelizeConfig, Sequelize, { ProductsCategory })
let ProductsVariants = require('./products_variants.model')(sequelizeConfig, Sequelize, { Products })
let Roles = require('./roles.model')(sequelizeConfig, Sequelize)
let Customers = require('./customers.model')(sequelizeConfig, Sequelize)
let Users = require('./users.model')(sequelizeConfig, Sequelize, { Roles, Customers })
let Address = require('./address.model')(sequelizeConfig, Sequelize)
let Orders = require('./orders.model')(sequelizeConfig, Sequelize)
let OrderProducts = require('./order_products.model')(sequelizeConfig, Sequelize)
let Payment = require('./payment.model')(sequelizeConfig, Sequelize)

Products.hasMany(ProductsVariants, { as: 'pv', foreignKey: 'products_id', sourceKey: 'id' })

db.ProductsCategory = ProductsCategory
db.Products = Products
db.ProductsVariants = ProductsVariants
db.Roles = Roles
db.Customers = Customers
db.Users = Users
db.Address = Address
db.Orders = Orders
db.OrderProducts = OrderProducts
db.Payment = Payment

module.exports = db

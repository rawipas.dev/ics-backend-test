module.exports = (sequelize, Sequelize, options = {}) => {
  const { ProductsCategory } = options
  const Model = sequelize.define(
    'products',
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
      },
      name: {
        type: Sequelize.STRING,
      },
      category_id: {
        type: Sequelize.INTEGER,
      },
      gender: {
        type: Sequelize.STRING,
      },
      visible: {
        type: Sequelize.BOOLEAN,
      },
      deleted: {
        type: Sequelize.BOOLEAN,
      },
      created_at: {
        type: Sequelize.DATE,
      },
      created_by: {
        type: Sequelize.INTEGER,
      },
      modified_at: {
        type: Sequelize.DATE,
      },
      modified_by: {
        type: Sequelize.INTEGER,
      },
    },
    {
      freezeTableName: true,
      timestamps: false,
    }
  )

  Model.hasMany(ProductsCategory, { as: 'pc', foreignKey: 'id', sourceKey: 'category_id' })

  return Model
}

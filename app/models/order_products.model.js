module.exports = (sequelize, Sequelize, options = {}) => {
  const Model = sequelize.define(
    'order_products',
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
      },
      order_id: {
        type: Sequelize.INTEGER,
      },
      products_id: {
        type: Sequelize.INTEGER,
      },
      products_variants_id: {
        type: Sequelize.INTEGER,
      },
      products_price: {
        type: Sequelize.DECIMAL,
      },
      products_discount: {
        type: Sequelize.DECIMAL,
      },
      products_vat: {
        type: Sequelize.DECIMAL,
      },
      quantity: {
        type: Sequelize.INTEGER,
      },
      created_at: {
        type: Sequelize.DATE,
      },
      created_by: {
        type: Sequelize.INTEGER,
      },
      modified_at: {
        type: Sequelize.DATE,
      },
      modified_by: {
        type: Sequelize.INTEGER,
      },
    },
    {
      freezeTableName: true,
      timestamps: false,
    }
  )

  return Model
}

module.exports = (sequelize, Sequelize, options = {}) => {
  const Model = sequelize.define(
    'products_variants',
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
      },
      products_id: {
        type: Sequelize.INTEGER,
      },
      name: {
        type: Sequelize.STRING,
      },
      amount: {
        type: Sequelize.INTEGER,
      },
      amount_used: {
        type: Sequelize.INTEGER,
      },
      price: {
        type: Sequelize.DECIMAL,
      },
      discount: {
        type: Sequelize.DECIMAL,
      },
      vat: {
        type: Sequelize.DECIMAL,
      },
      visible: {
        type: Sequelize.BOOLEAN,
      },
      deleted: {
        type: Sequelize.BOOLEAN,
      },
      created_at: {
        type: Sequelize.DATE,
      },
      created_by: {
        type: Sequelize.INTEGER,
      },
      modified_at: {
        type: Sequelize.DATE,
      },
      modified_by: {
        type: Sequelize.INTEGER,
      },
    },
    {
      freezeTableName: true,
      timestamps: false,
    }
  )
  return Model
}

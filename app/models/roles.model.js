module.exports = (sequelize, Sequelize, options = {}) => {
  const Model = sequelize.define(
    'roles',
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
      },
      name: {
        type: Sequelize.STRING,
      },
      created_at: {
        type: Sequelize.DATE,
      },
      created_by: {
        type: Sequelize.INTEGER,
      },
      modified_at: {
        type: Sequelize.DATE,
      },
      modified_by: {
        type: Sequelize.INTEGER,
      },
    },
    {
      freezeTableName: true,
      timestamps: false,
    }
  )

  return Model
}

module.exports = (sequelize, Sequelize, options = {}) => {
  const Model = sequelize.define(
    'orders',
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
      },
      order_code: {
        type: Sequelize.STRING,
      },
      users_id: {
        type: Sequelize.INTEGER,
      },
      address_id: {
        type: Sequelize.INTEGER,
      },
      order_price: {
        type: Sequelize.DECIMAL,
      },
      order_discount: {
        type: Sequelize.DECIMAL,
      },
      order_vat: {
        type: Sequelize.DECIMAL,
      },
      status: {
        type: Sequelize.INTEGER,
      },
      created_at: {
        type: Sequelize.DATE,
      },
      created_by: {
        type: Sequelize.INTEGER,
      },
      modified_at: {
        type: Sequelize.DATE,
      },
      modified_by: {
        type: Sequelize.INTEGER,
      },
    },
    {
      freezeTableName: true,
      timestamps: false,
    }
  )

  return Model
}
